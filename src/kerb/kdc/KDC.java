/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kerb.kdc;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SecretKey;
import kerb.util.CipherController;
import kerb.util.Message;
import kerb.util.Network;
import kerb.util.Ticket;
import kerb.util.Util;

/**
 *
 * @author Lucas
 */
public class KDC extends Thread {

    private HashMap<String, SecretKey> keys;
    private Network net;
    private final long dealine = 60000;

    public KDC(int p) {
        keys = new HashMap<String, SecretKey>();
        net = new Network("[KDC]", p);
    }

    public void initializeKDC() {
        net.initializeServer();
        /*
         Initialize the Authenticate Server (AS)
         */
        createKey(Util.CLIENT);
        createKey(Util.SERVICE);
        createKey(Util.AS);
        createKey(Util.TGS);
    }

    public void run() {
        /*
         AUTHENTICATE SERVER (AS)
         */
        System.out.println("[KDC] Initializing Authentication Server(AS)...");
        System.out.println("[KDC] Waiting Connections...");
        net.waitingConnection();
        System.out.println("[KDC] Waiting Message M1...");
        Message m1 = net.waitingMessage();
        System.err.println(m1.toStringM1());

        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            Logger.getLogger(KDC.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*
         Creating the Message M2 
         */
        System.out.println("[KDC] Creating Message M2...");
        SecretKey sessionKey = CipherController.getKey(Util.CLIENT + Util.TGS);
        Message m2 = new Message(sessionKey, m1.getClientID(), dealine, m1.getRandomNumber());
        m2.initializeM2(getKey(Util.CLIENT), getKey(Util.TGS));
        System.err.println(m2.toStringM2());
        System.out.println("[KDC] Sending...");
        net.sendMessage(m2);

        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            Logger.getLogger(KDC.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*
         Initialize the Ticket Granting Service(TGS)
         */
        System.out.println("[KDC] Waiting Message M3...");
        Message m3 = net.waitingMessage();
        System.err.println(m3.toStringM3());
        /*
         Validating Ticket TGT
         */
         try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                Logger.getLogger(KDC.class.getName()).log(Level.SEVERE, null, ex);
            }
        System.out.println("[KDC] Validating...");
        if (CipherController.decrypt(m3.getTicketServer(), getKey(Util.TGS)) != null) {
           
            /*
             Creating Message M4 
             */
            System.out.println("[KDC] Creating Message M4...");
            SecretKey sk = CipherController.getKey(Util.CLIENT + Util.SERVICE);
            Ticket tk = new Ticket(Util.CLIENT, dealine, sk);
            Message m4 = new Message(sessionKey, m3.getRandomNumber(), tk);
            m4.initializeM4(getKey(Util.SERVICE));
            System.err.println(m4.toStringM4());
            System.out.println("[KDC] Sending...");
            net.sendMessage(m4);
        }
    }

    public void createKey(String id) {
        keys.put(id, CipherController.getKey(id));
    }

    public SecretKey getKey(String id) {
        return keys.get(id);
    }

}
