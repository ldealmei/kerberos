/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kerb;

import kerb.client.Client;

/**
 *
 * @author Lucas
 */
public class MainClient {
    
    public static void main(String[] args) {
         /**
         * Start the Client
         */
        System.out.println("[Main] Initializing Client...");
        Client client = new Client("localhost", 5555, "localhost", 6666);
        client.start();
    }
   
}
