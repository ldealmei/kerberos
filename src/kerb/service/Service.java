/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kerb.service;

import javax.crypto.SecretKey;
import kerb.util.CipherController;
import kerb.util.Message;
import kerb.util.Network;
import kerb.util.Ticket;
import kerb.util.Util;

/**
 *
 * @author Lucas
 */
public class Service extends Thread {
    
    private Network net;
    private SecretKey key;
    
    public Service(int p){
        net = new Network("Service]",p);
        net.initializeServer();
        key = CipherController.getKey(Util.SERVICE);
    }
    
    @Override
    public void run(){
        System.out.println("[Service] Waiting Connection...");
        net.waitingConnection();
        
        System.out.println("[Service] Waiting Message M5...");
        Message m5 = net.waitingMessage();
        System.err.println(m5.toStringM5());
        
        Ticket p1 = CipherController.decrypt(m5.getTicketServer(), key);
        System.out.println("[Service] Validating...");
        if(p1 != null & p1.getClientID().equalsIgnoreCase(m5.getClientID())){
            
            System.out.println("[Service] Request: "+m5.getRequest());
            
            System.out.println("[Service] Creating Message M6...");
            Message m6 = new Message("I'm fine, thanks!", p1.getSessionKey());
            m6.initializeM6();
            System.err.println(m6.toStringM6());
            System.out.println("[Service] Sending...");
            net.sendMessage(m6);
        }
                
    }
}
