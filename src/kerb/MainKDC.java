/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kerb;

import kerb.kdc.KDC;

/**
 *
 * @author Lucas
 */
public class MainKDC {
    
    public static void main(String[] args) {
          /**
         * Initialize the KDC + Services
         */
        System.out.println("[Main] Initializing KDC...");
        KDC kdc = new KDC(5555);
        kdc.initializeKDC();
        kdc.start();
    }
    
}
