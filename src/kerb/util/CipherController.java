/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kerb.util;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;

/**
 *
 * @author Lucas
 */
public class CipherController {

    public static SealedObject encrypt(Ticket tk, SecretKey sk) {
        Cipher ecipher;
        SealedObject encrypted;
        try {
            ecipher = Cipher.getInstance("DES");
            ecipher.init(Cipher.ENCRYPT_MODE, sk);
            encrypted = new SealedObject(tk, ecipher);
            return encrypted;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IOException | IllegalBlockSizeException ex) {
            Logger.getLogger(CipherController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Ticket decrypt(SealedObject tk, SecretKey sk) {
        Ticket decrypted;
        try {
            decrypted = (Ticket) tk.getObject(sk);
            return decrypted;
        } catch (NoSuchAlgorithmException | InvalidKeyException | IOException | ClassNotFoundException ex) {
            Logger.getLogger(CipherController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static SecretKey getKey(String id) {
        try {
            KeyGenerator kg = KeyGenerator.getInstance("DES");
            SecureRandom sn = new SecureRandom(id.getBytes());
            kg.init(sn);
            return kg.generateKey();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CipherController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
