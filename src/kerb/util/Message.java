/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kerb.util;

import java.io.Serializable;
import java.util.Date;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;

/**
 * Message is a class that represents all possibilities of messages in Kerberos, keeping their information.
 * 
 * @author Lucas
 */
public class Message implements Serializable{
    /**
     * It's client identification
     */
    private String clientID;
    /**
     * It's server identification
     */
    private String serverID;
    /**
     * It's a service identification
     */
    private String serviceID;
    /**
     * It's a deadline time for one service
     */
    private long deadline;
    /**
     * It's a random number
     */
    private int randomNumber;
    /**
     * It's a encrypted session message contain some information
     */
    private SecretKey sessionKey;
    /**
     * It's a ticket for this message
     */
    private Ticket ticket;
    
    private SealedObject ticketServer;
    
    /**
     * It's the current date in your Time Zone
     */
    private Date currentDate;
    /**
     * It's the request information in detailed
     */
    private String request;
    /**
     * It's the final response from the service server
     */
    private String response;
    
    
    private SealedObject m2Part1;
    private SealedObject m2Part2;
    private SealedObject m3;
    private SealedObject m4Part1;
    private SealedObject m4Part2;
    private SealedObject m5;
    private SealedObject m6;
    
    
    /**
     * M1 - Message from Client to Authentication Service(AS)
     * 
     * @param c
     * @param tgs
     * @param ts
     * @param n 
     */
    public Message(String c,String tgs,long ts,int n){
        this.clientID = c;
        this.serviceID = tgs;
        this.deadline = ts;
        this.randomNumber = n;
    }
    
    public String toStringM1(){
        return "Message M1[\nclientID(c): "+clientID+";\nserviceID(tgs): "+serviceID+";\ndeadline(ts): "+deadline+";\nrandomNumber(n): "+randomNumber+"]";
    }
    
    /**
     * M2 - Message from Authentication Service(AS) to Client
     * 
     * @param sk
     * @param c
     * @param d 
     * @param n 
     */
    public Message(SecretKey sk, String c, long  d, int n){
        this.sessionKey = sk;
        this.clientID = c;
        this.deadline = d;
        this.randomNumber = n;
    }
    
     public String toStringM2(){
        return "Message M2[\nsessionKey(sk): "+sessionKey+";\nclientID(c): "+clientID+";\ndeadline(ts): "+deadline+";\nrandomNumber(n): "+randomNumber+"]";
    }
    
    /**
     * M3 - Message from Client to Ticket Granting Service(TGS)
     * 
     * @param c
     * @param dt
     * @param tk
     * @param s
     * @param n 
     */
    public Message(String c, Date dt, SealedObject tk, String s, int n){
        this.clientID = c;
        this.currentDate = dt;
        this.ticketServer = tk;
        this.serverID = s;
        this.randomNumber = n;
    }
    
      public String toStringM3(){
        return "Message M3[\nclientID(c): "+clientID+";\ncurrentDate(dt): "+currentDate+";\nticketServer(tk): "+ticketServer+";\nserverID(s): "+serverID+";\nrandomNumber(n): "+randomNumber+"]";
    }
    
    /**
     * M4 - Message from TGS to Client
     * @param sk
     * @param n
     * @param tk 
     */
    public Message(SecretKey sk,int n, Ticket tk){
        this.sessionKey = sk;
        this.randomNumber = n;
        this.ticket = tk;
    }
    
      public String toStringM4(){
        return "Message M4[\nsessionKey(sk): "+sessionKey+";\nrandomNumber(n): "+randomNumber+";\nticket(tk): "+ticket+"]";
    }
    
    /**
     * M5 - Message from Client to Service Server
     * 
     * @param c
     * @param dt
     * @param tks
     * @param rq 
     * @param sk
     */
    public Message(String c, Date dt, SealedObject tks,String rq,SecretKey sk){
        this.clientID = c;
        this.currentDate = dt;
        this.ticketServer = tks;
        this.request = rq;
        this.sessionKey = sk;
    }
    
    public String toStringM5(){
        return "Message M5[\nclientID(c): "+clientID+";\ncurrentDate(dt): "+currentDate+";\nticketServer(tk): "+ticketServer+";\nrequest(rq): "+request+";\nsessionKey(n): "+sessionKey+"]";
    }
    
    /**
     * M6 - Message from Service Server to Client
     * 
     * @param rp
     * @param sk 
     */
    public Message(String rp, SecretKey sk){
        this.response = rp;
        this.sessionKey = sk;
    }
    
     public String toStringM6(){
        return "Message M6[\nresponse(rp): "+response+";\nsessionKey(sk): "+sessionKey+";\nticket(tk): "+ticket+"]";
    }

    /**
     * M2 is initialized
     * 
     * @param kc
     * @param ktgs 
     */
    public void initializeM2(SecretKey kc,SecretKey ktgs){
        Ticket t1 = new Ticket(sessionKey, randomNumber);
        Ticket t2 = new Ticket(clientID, deadline, sessionKey);
        m2Part1 = CipherController.encrypt(t1, kc);
        m2Part2 = CipherController.encrypt(t2, ktgs);
    }
    
    /**
     * M3 is initialized
     */
    public void initializeM3(){
        Ticket tk = new Ticket(clientID, currentDate);
        m3 = CipherController.encrypt(tk, sessionKey);
    }
    
    /**
     * M4 is initialized
     * 
     * @param ks 
     */
    public void initializeM4(SecretKey ks){
        Ticket tk = new Ticket(sessionKey, randomNumber);
        m4Part1 = CipherController.encrypt(tk, sessionKey);
        m4Part2 = CipherController.encrypt(ticket, ks);
    }
    
    /**
     * M5 is initialized
     */
    public void initializeM5(){
        Ticket tk = new Ticket(clientID, currentDate);
        m5 = CipherController.encrypt(tk, sessionKey);
    }
    
    /**
     * M6 is initialized
     */
    public void initializeM6(){
        Ticket tk = new Ticket(response);
        m6 = CipherController.encrypt(tk, sessionKey);
    }   
    
    public String getClientID() {
        return clientID;
    }

    public String getServiceID() {
        return serviceID;
    }

    public long getDeadline() {
        return deadline;
    }

    public int getRandomNumber() {
        return randomNumber;
    }

    public SecretKey getSessionKey() {
        return sessionKey;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public Date getCurrentDate() {
        return currentDate;
    }

    public String getRequest() {
        return request;
    }

    public String getResponse() {
        return response;
    }

    public SealedObject getM2Part1() {
        return m2Part1;
    }

    public SealedObject getM2Part2() {
        return m2Part2;
    }

    public String getServerID() {
        return serverID;
    }

    public SealedObject getM3() {
        return m3;
    }

    public SealedObject getM4Part1() {
        return m4Part1;
    }

    public SealedObject getM4Part2() {
        return m4Part2;
    }

    public SealedObject getM5() {
        return m5;
    }

    public SealedObject getM6() {
        return m6;
    }

    public void setSessionKey(SecretKey sessionKey) {
        this.sessionKey = sessionKey;
    }

    public SealedObject getTicketServer() {
        return ticketServer;
    }

    
    
}
