/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kerb.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import kerb.MainService;

/**
 * Network is a class that connects in a socket or creates a server socket
 *
 * @author Lucas
 */
public class Network {

    /**
     * It's the IP address in he Server
     */
    private String HOST;
    /**
     * It's a number of the port in Server
     */
    private int PORT;
    /**
     * It's a socket connection between server and client or the client
     */
    private Socket clientSocket;
    /**
     * It's server socket connection thar allows other client to connect
     */
    private ServerSocket serverSocket;
    /**
     * It's output stream of data between client-server
     */
    private ObjectOutputStream out;
    /**
     * It's a input stream of data between client-server
     */
    private ObjectInputStream in;
    
    private String name;

    /**
     * Used to create a Server
     *
     * @param p
     */
    public Network(String name,int p) {
        this.PORT = p;
        this.name = name;
    }

    /**
     * Used to create a Client
     *
     * @param ip
     * @param p
     */
    public Network(String name,String ip, int p) {
        this.HOST = ip;
        this.PORT = p;
        this.name = name;
    }

    /**
     * Used to initialize a client
     */
    public void initializeClient() {
        try {
            clientSocket = new Socket(HOST, PORT);
            out = new ObjectOutputStream(clientSocket.getOutputStream());
            in = new ObjectInputStream(clientSocket.getInputStream());
        } catch (UnknownHostException eS) {
            System.err.println(MainService.getTimestamp() +name+ "Don't know about host: " + HOST);
            System.exit(1);
        } catch (IOException e) {
            System.err.println(MainService.getTimestamp() +name+ "Couldn't get I/O for "
                    + "the connection to: " + HOST);
            System.exit(1);
        }
    }

    /**
     * Initialize the server side
     */
    public void initializeServer() {
        try {
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            System.err.println(MainService.getTimestamp() + name+ " Could not listen on port: " + PORT);
            System.exit(1);
        }
    }

    /**
     * Wait a message from server/client
     * 
     * @return message
     */
    public Message waitingMessage() {
        try {
            return (Message) in.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Network.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void waitingConnection(){
        try {
            clientSocket = serverSocket.accept();
            out = new ObjectOutputStream(clientSocket.getOutputStream());
            in = new ObjectInputStream(clientSocket.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(Network.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Send a message to server/client
     * 
     * @param msg 
     */
    public void sendMessage(Message msg) {
        try {
            out.writeObject(msg);
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(Network.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Close all the streams
     */
    public void closeStream(){
        try {
            in.close();        
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(Network.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    /**
     * close the client connection
     */
    public void closeClient(){
        try {
            clientSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(Network.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Close all the connections with the server
     */
    public void closeServer(){
        try {
            serverSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(Network.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getHOST() {
        return HOST;
    }

    public int getPORT() {
        return PORT;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public ObjectOutputStream getOut() {
        return out;
    }

    public ObjectInputStream getIn() {
        return in;
    }
    
    
}
