/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kerb.util;

import java.io.Serializable;
import java.util.Date;
import javax.crypto.SecretKey;

/**
 *
 * @author Lucas
 */
public class Ticket implements Serializable {
    
    private String clientID;
    private long dealine;
    private SecretKey sessionKey;
    private int randomNumber;
    private Date currentDate;
    private String response; 

    public Ticket(String response) {
        this.response = response;
    }

    public Ticket(String clientID, Date currentDate) {
        this.clientID = clientID;
        this.currentDate = currentDate;
    }
    
    public Ticket(SecretKey sk, int n){
        this.sessionKey = sk;
        this.randomNumber = n;
    }

    public Ticket(String clientID, long dealine, SecretKey sessionKey) {
        this.clientID = clientID;
        this.dealine = dealine;
        this.sessionKey = sessionKey;
    }

    public String getClientID() {
        return clientID;
    }

    public long getDealine() {
        return dealine;
    }

    public SecretKey getSessionKey() {
        return sessionKey;
    }

    public int getRandomNumber() {
        return randomNumber;
    }

    public Date getCurrentDate() {
        return currentDate;
    }

    public String getResponse() {
        return response;
    }

    @Override
    public String toString() {
        return "Ticket{" + "clientID=" + clientID + ", dealine=" + dealine + ", sessionKey=" + sessionKey + ", randomNumber=" + randomNumber + ", currentDate=" + currentDate + ", response=" + response + '}';
    }
    
      
    
}
