/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kerb.client;

import java.util.Date;
import java.util.Random;
import java.util.Scanner;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import kerb.util.CipherController;
import kerb.util.Message;
import kerb.util.Network;
import kerb.util.Ticket;
import kerb.util.Util;

/**
 *
 * @author Lucas
 */
public class Client extends Thread {
    
    private Network kdc;
    private Network service;
    private Message message;
    private SecretKey key;
    private String tgs;
    private long deadline;
    
    public Client(String kip, int kp, String sip, int sp){
        this.kdc = new Network("[Client-KDC]", kp);
        this.service = new Network("[Client-Service]", sip, sp);
        key = CipherController.getKey(Util.CLIENT);        
    }   
    
    @Override
    public void run(){        
        /*
            Ask the client which service he want
        */
        Scanner s = new Scanner(System.in);
        //System.out.println("[Client] Which service Do you want?");
        //tgs = s.nextLine();
        tgs = "Teste";
//        System.out.println("[Client] How much time(seconds) could you wait?");
//        deadline = s.nextLong()*1000;
        deadline = 60000;
        deadline += System.currentTimeMillis();
        Random rd = new Random();
        int n1 = rd.nextInt();
        
        /*
        Prepare the message M1
        */
        System.out.println("[Client] Creating Message M1...");
        Message m1 = new Message(Util.CLIENT, tgs, deadline, n1);
        System.err.println(m1.toStringM1());        
        kdc.initializeClient();
        
        /*
            Comunnicate with Authenticte Service(AS)
        */    
        System.out.println("[Client] Sending...");
        kdc.sendMessage(m1);
        
        System.out.println("[Client] Waiting Message M2...");
        Message m2 = kdc.waitingMessage();
        System.err.println(m2.toStringM2());
        
        SealedObject m2p1 = m2.getM2Part1();
        SealedObject m2p2 = m2.getM2Part2();
        
//        System.out.println(m2p1);
        Ticket p1 = CipherController.decrypt(m2p1, key);
        
        SecretKey sessionKey = p1.getSessionKey();
        //check if is the correct answer
        System.out.println("[Client] Validating...");
        if(p1.getRandomNumber() != n1){
            System.out.println("[Client] The communication will be closed");
            System.exit(0);
        }
        
        /*
            Communicate with Ticket Granting Service(TGS)
        */
        
        int n2 = (int) Math.random();
        System.out.println("[Client] Creating Message M3...");
        Message m3 = new Message(Util.CLIENT, new Date(), m2p2, Util.SERVICE, n2);
        m3.setSessionKey(sessionKey);
        m3.initializeM3();
        System.err.println(m3.toStringM3());
        System.out.println("[Client] Sending...");
        kdc.sendMessage(m3);
        
        System.out.println("[Client] Wating Message M4...");
        Message m4 = kdc.waitingMessage();
        System.err.println(m4.toStringM4());
        
        SealedObject m4p1 = m4.getM4Part1();
        SealedObject m4p2 = m4.getM4Part2();
        
        p1 = CipherController.decrypt(m4p1, sessionKey);
        System.out.println("[Client] Validating...");        
        if(p1.getRandomNumber() != n2){
            System.out.println("[Client] The communication will be closed");
            System.exit(0);
        }
        
        /*
            Communicate with the Service Server
        */
        System.out.println("[Client] Creating Message M5...");       
        Message m5 = new Message(Util.CLIENT, new Date(), m4p2, "Hi Server, How are you?",p1.getSessionKey());
        m5.initializeM5();
        System.err.println(m5.toStringM5());
        System.out.println("[Client] Sending...");
        service.initializeClient();
        service.sendMessage(m5);
       
        
        System.out.println("[Client] Waiting Message M6...");
        Message m6 = service.waitingMessage();
        
        SealedObject m6p1 = m6.getM6();
        p1 = CipherController.decrypt(m6p1, m6.getSessionKey());
        
        System.out.println("[Client] Response: "+p1.getResponse());
        
    }
}
