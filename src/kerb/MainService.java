/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kerb;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import kerb.client.Client;
import kerb.kdc.KDC;
import kerb.service.Service;

/**
 *
 * @author Lucas
 */
public class MainService {

    public static void main(String[] args) {         
        System.out.println("[Main] Initializing Service...");
        Service s = new Service(6666);
        s.start();     
        
    }

    public static String getTimestamp() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm ");
        Date date = new Date();
//        System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48
        return dateFormat.format(date);
    }

}
